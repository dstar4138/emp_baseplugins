#
# Makefile for building core EMP plugins.
#

EMP_INCLUDE = ../apps/emp_core/include
EMP_EBIN = ../apps/emp_core/ebin

all: clean $(EMP_INCLUDE) $(EMP_EBIN)
	mkdir ebin
	erlc -I $(EMP_INCLUDE) -pa $(EMP_EBIN) -o ebin/ */src/*.erl

clean:
	rm -rf ebin

