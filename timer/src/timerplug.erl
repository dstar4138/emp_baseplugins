-module(timerplug).
-behaviour( emp_plugin ).
-include_lib("emp_core/include/emptypes.hrl").

% Emp plugin required functions.
-export([register_cmds/0, register_events/0, registration_info/0]).
% Emp Optional functions.
-export([init/1,finish/1]).

% Plugin Commands.
-export([add_timer/4,random_timer/1,kill_timer/1]).

% Module functions.
-export([trigger_end_timer/2]).

-define(MAX_RANDOM, 80).
-define(TIMER_END_EVENT, <<"timer_end">>).

%%
%% ===========================================================================
%% Public EMP Plugin API
%% ===========================================================================
%%

registration_info() ->
	{timerplug, 
	 [ {initial_cmd, <<"timer">>},  %User can use --target=timer
	   {local_global, global},      %Doesn't neeed a particular comp. (!local)
	   {sys_user, user}	            %User level plugin 
	 ]}.


register_cmds() -> 
	[
	 %%% TODO: Expand on the current functionality once the API has been formalized %%%
	 ?EMPCOMMAND( <<"add">>, fun add_timer/4, 
			   [?EMPPARAM(<<"seconds">>,?EMP_NUMBER,0),
				?EMPPARAM(<<"minutes">>,?EMP_NUMBER,0),
				?EMPPARAM(<<"hours">>,?EMP_NUMBER,0)
			   ], ?EMP_BOOLEAN),
	 ?EMPCOMMAND( <<"arand">>, fun random_timer/1, [], ?EMP_NUMBER ),
	 ?EMPCOMMAND( <<"kill">>, fun kill_timer/1, [], ?EMP_NUMBER)
	 ].


register_events() -> 
	[
	 ?EMPEVENT( ?TIMER_END_EVENT, [ ?EMPPARAM(<<"time">>, ?EMP_NUMBER, 0) ])
	].

%Ignore old state, start it up
init( State ) ->
	timer:start(), 
    emp_plugin:set_var(State, "timers", []), %Empty list is new state.
    ok.

% Clean up all the timers and cancel them all before closing up.
% Will return [] at the end and that will be saved as new state.
finish( State ) -> %State in this case is just the Pid of the PlugRunner.
	case emp_plugin:get_var(State, "timers") of
		{error, _} -> ok;
		{ok, Rest} -> cancel_all(Rest)
	end, 
    ok.
cancel_all([]) -> [];
cancel_all([Timer|Rest]) ->
	timer:cancel(Timer), cancel_all(Rest).


%%
%% ===========================================================================
%% Plugin Commands
%% ===========================================================================
%%

add_timer(State, Seconds, Minutes, Hours) ->
	Time = Seconds + (60*Minutes)+(120*Hours),
	add_timer(State, Time).

% Add a timer for the number of seconds listed.
add_timer(State, Seconds) -> 
	Time = timer:seconds(Seconds),
	case timer:apply_after(Time, ?MODULE, trigger_end_timer, [State, Time])
	of 
		{ok, Ref} -> 
			case emp_plugin:update_var(State, "timers", fun (List) -> [Ref|List] end)
            of
                {ok, _Val} -> true;
                Error -> Error
            end;
		Err -> {error, Err}
	end.

%% Adds a random timer to the state list.
random_timer(State) ->
	Random = random:uniform(?MAX_RANDOM),
	Ret = add_timer(State, Random),
	case Ret of 
		true -> Random;
		_    -> Ret
	end.

%% Kills the top timer in the state.
kill_timer(State) -> 
	case emp_plugin:get_var(State, "timers") of
		{ok,[]} -> {error,<<"No Timers">>};
		{ok,[Timer|Rest]} ->
			timer:cancel(Timer),
			emp_plugin:set_var(State, "timers", Rest),
			true;
        Error -> Error
	end.
	

%%
%% ============================================================================
%% Private Functions (only used internally)
%% ============================================================================
%%

trigger_end_timer(State, Time) ->
	emp_plugin:trigger_event(State, ?TIMER_END_EVENT, [Time]).
